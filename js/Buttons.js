function MyInput(obj){
    this._input = obj;

    this.setValue = function(value){
        this._input.value = value;
    }
    this.setTooltip = function(tip){
        this._input.dataset.tooltip = tip;
    }
}

function SwitchButton(obj, btnName){
    console.log('swbt '+btnName);
    MyInput.apply(this, arguments);
    var name = btnName;
    var self = this;

    this.update = function(status){
        // console.log(status);
        if(status){
            setBtn(this.btnValue[name][1], this.btnTooltip[name][1]);
        } else {
            setBtn(this.btnValue[name][0], this.btnTooltip[name][0]);
        }
    }
    function setBtn(value, tip){
    // function setBtn(value,tip){
        // console.log('val: '+value+'  name: '+name);
        self.setValue(value);
        self.setTooltip(tip);
    }
}
// SwitchButton.prototype = Object.create(new MyInput);

function FieldSpeed(obj, maxSpeed, minSpeed){
    MyInput.apply(this, arguments);
    var max = maxSpeed;
    var min = minSpeed;
    this.setTooltip("Укажите скорость от "+min+" до "+max);

    this.inputSpeed = function(item){
        var sp = this._input.value;    //получили введенную скорость
        // var maxsp = 10, minsp = 1;
        if ((sp <= max)&&(sp >= min)){
            setSpeed(sp);//устанавливаем, если это число в пределах разумного
        } else if (sp > max){//если больше - ставить максимум
            setSpeed(max);
        } else if (sp < min){
            setSpeed(min);//если меньше - ставить минимум
        }
        //если введено не число - вернется старая скорость
        this.update(item);

        function setSpeed(speed){
            item.setSpeed(speed);
            item.tryAnimation();
        }
    }
    this.update = function(item){
        this.setValue(item.getSpeed());
    }
}
// FieldSpeed.prototype = Object.create(new MyInput);

SwitchButton.prototype.btnValue = {
    "start": ["START", "STOP "],
    "route": ["->", "<-"]
}
SwitchButton.prototype.btnTooltip = {
    "start": ["Запустить", "Остановить"],
    "route": ["Изменить направление анимации в обратную сторону", "Вернуть прямое направление анимации"]
}
// SwitchButton.prototype.test = 'qqqq';
