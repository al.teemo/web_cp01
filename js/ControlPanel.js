
function ControlPanel(st, rt, txt, sp, maxsp, minsp){
    var current = -1;
    // console.log(st);
    var btnStart = new SwitchButton(st, "start");
    // console.log(btnStart);
    var btnRoute = new SwitchButton(rt, "route");
    var textInfo = txt;
    var fldSpeed = new FieldSpeed(sp, maxsp, minsp);

    var item = [];

    this.eventBtnStartClicked = function(){
        item[current].reverseSliding();
        this.updateBtnStart();
        this.tryAnimation();
        document.onmouseout(getByName('bTextStart')[0]);
    }
    this.eventBtnRouteClicked = function(){
        item[current].reverseRoute();
        this.updateBtnRoute();
        // tryAnimation();
        document.onmouseout(getByName('bTextRoute')[0]);
    }
    this.eventInputSpeed = function(){
        fldSpeed.inputSpeed(item[current]);
    }
    
    this.update = function(){
        // console.log('upd panel');
        this.updateBtnStart();
        this.updateBtnRoute();
        this.updateSpeed();
        this.updateText();
        // this.tryAnimation();
    }

    this.select = function(num){
        this.updateBorder(num);
        this.update();
    }
    this.updateBorder = function(num){
        item[current].unselected();
        item[num].selected();
        current = num;
    }

    this.updateItemValue = function(){
        item[current].updateValue();
    }
    this.saveCurrentValue = function(){
        item[current].parseValue();
    }

    this.tryAnimation = function(){
        // console.log(item[current]);
        item[current].tryAnimation();
    }
//////////////////////
    this.addItem = function(obj, speed, sliding, step, value){
        item.push(obj);
        // console.log('get in panel: '+value.length);
        if(current == -1)
            current = 0;
        item[item.length-1].init(speed, sliding, step, value);
        // item[item.length-1].myprt('after: ');
        // console.log('---');
        
        var temp = current;
        this.updateBorder(item.length-1);
        this.tryAnimation();
        this.updateBorder(temp);
    }

    this.updateBtnStart = function(){
        btnStart.update(item[current].getSliding());
    }
    this.updateBtnRoute = function(){
        btnRoute.update(item[current].getRoute() == 1);
    }
    this.updateSpeed = function(){
        fldSpeed.update(item[current]);
    }
    this.updateText = function(){
        textInfo.innerHTML = "Бегущая строка ("+(current+1)+")";
    }

    this.getCurrentValue = function(){
        return item[current].getValue();
    }
    this.setCurrentValue = function(str){
        item[current].inputValue(str);
    }
    this.getCurrentSliding = function(){
        return item[current].getSliding();
    }
    this.setCurrentSliding = function(sl){
        item[current].setSliding(sl);
    }
//--------------------------------
    this.test = function(){
        console.log(btnStart);
        console.log(btnRoute);
        console.log(textInfo);
        console.log(fldSpeed);
    }
}
