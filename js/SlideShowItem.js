function SlideShowItem(newObj){
    // console.log('ssitem ');
    this._obj = newObj;
    var speed;
    this._delay;
    var sliding;
    var timerId = -1;
    this._route;
    this._value;
    var self = this;

    this.inputValue;
    this.nextFrame;
    // () => {console.log('eeee');}

    this.init = function(sp, sl, rt, val){
        // console.log(this._obj.firstElementChild)
        speed = sp;
        sliding = sl;
        self._route = rt;
        self._value = val;
        // console.log('see in common: '+this._value.length);
        // console.log(this);
        // this.myprt('init par: ');
        // this.updateValue();
        // return this;
    }

    this.tryAnimation = function(){
        // console.log(this);
        // this.myprt();
        if(sliding){
            if(timerId != -1) //если обновление кадра запланировано -
                clearTimeout(timerId); // отменить его
            timerId = setTimeout(self.tryAnimation, self._delay/speed);//запланировать обновление
            self.nextFrame();//изменить кадр
        } else {
            timerId = -1;  //пометить, что обновление не запланировано
        }
    }

    this.unselected = function(){
        this._obj.style.borderColor = this.unSelectColor;
        // console.log('unselect: '+this._obj.name);
    }
    this.selected = function(){
        this._obj.style.borderColor = this.isSelectColor;
        // console.log('select: '+this._obj.name);
    }
    this.reverseSliding = function(){
        sliding = !sliding;
    }
    this.reverseRoute = function(){
        this._route *= -1;
    }


    this.setSpeed =   function(sp){   speed = sp;}
    this.setSliding = function(sl){   sliding = sl;}
    this.setRoute =    function(st){   this._route = st;}
    this.setValue =   function(str){  this._value = str;}

    this.getSpeed = function(){     return speed;}
    this.getSliding = function(){   return sliding;}
    this.getRoute = function(){      return this._route;}
    this.getValue = function(){     return this._value;}

//-----------------------------------
    this.test = function(){
        // this.myprt();
    }
    // this.myprt = function(){
    //     console.log(this._value);
    // }
}

SlideShowItem.prototype.isSelectColor = "#b00";
SlideShowItem.prototype.unSelectColor = "#111";
