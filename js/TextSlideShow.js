function TextSlideShow(obj){
    SlideShowItem.apply(this, arguments);
    this._delay = 1000;
    // this.remembered;

    this.nextFrame = function(){
        var str = this._obj.value;

        if(this._route == 1)  //сдвигать строку в заданном направлении (1: прямо, -1: обратно)
            str = str.slice(1, str.length) + str[0];
        else
            str = str[str.length-1] + str.slice(0, str.length-1);
            
        this._obj.value = str;//обновить кадр
    }
    this.updateValue = function(){
        this.inputValue(this._value);
    }
    this.parseValue = function(){
        this._value = this._obj.value;
    }
    this.inputValue = function(str){
        this._obj.value = str;
    }
    
    // var parInit = this.init;
    // this.init = function(sp, sl, rt, val){
    //     parInit(sp, sl, rt, val);

    // }

//----------------------------------
    this.test2 = function(){
        this.print();
    }
}

// TextSlideShow.prototype = Object.create(new SlideShowItem);
