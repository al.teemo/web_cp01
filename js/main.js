//TODO: switch to classes
//TODO: грамотное движение текста

//запуск при открытии страницы
function bodyStart(){
    startTime();//запуск таймера
    var myTextList = ["При наведении на кнопки и слайды вылезают подсказки!", "123456789 10 11", "abcd"];
    btnValue = ["START", "STOP"];
    btnTooltipValue = ["Запустить", "Остановить"];
    btnRoute = ["->", "<-"];
    btnTooltipRoute = ["Сменить направление анимации в обратную сторону", "Вернуть прямое направление анимации"];
    isSelectColor = "#b00";
    unSelectColor = "#111";

    currentText = 0; //выбранная строка
    currentImg = 0;  //выбранная понорама

    //setup TEXT props
    speedText = [];
    slidingText = [];
    timerIdText = [];
    stepText = [];
    textValue = [];
    for(var i=0; i<3; i++){
        speedText.push(5-i*1.5);        //установить скорость
        slidingText.push(true);         //запускать бегущую строку
        timerIdText[i] = -1;            //сдвиг строки еще не запланирован
        stepText[i] = Math.pow(-1, i);  //направление строки
        textSetValue(i, myTextList[i]); //заполнить строку
        textValue.push(textGetValue(i));//запомнить введенную строку
        textGetFocus(i);    //установить фокус (чтобы заполнить строку и обновить панель)
        textLostFocus();    //убрать фокус (чтобы запустить анимацию)
    }

    //setup IMG props
    speedImg = [];
    slidingImg = [];
    timerIdImg = [];
    stepImg = [];
    imgValue = [
        ["img/01/image1.jpg", "img/01/image2.jpg", "img/01/image3.jpg", "img/01/image7.jpg", "img/01/image9.jpg", "img/01/image10.jpg"],
        ["img/02/image7.jpg", "img/02/image9.jpg"],
        ["img/03/image5.jpg", "img/03/image4.jpg", "img/03/image6.jpg", "img/03/image8.jpg"]
    ];
    for(var i=0; i<3; i++){
        speedImg.push(4 - i*1.5);       //установить скорость
        slidingImg.push(false);         //не запускать анимацию
        timerIdImg[i] = -1;             //смена кадра не запланирована
        stepImg[i] = Math.pow(-1, i);   //направление слайд-шоу
        imgSetValue(i, imgValue[i][0]); //отобразить первую картинку из набора
        imgSlideShow(i);                //попытаться запустить слайдшоу
    }
    imgClicked(0);                      //переместить фокус на первый слайд
    
    item = new TextSlideShow("hello");
    // item = new SlideShowItem("hello");
    item.setSpeed(999);
    // item2 = new SlideShowItem("second");
    item.test();
    // console.log(item);
}

function getByName(name){
    return document.getElementsByName(name)[0];
}
function getById(id){
    return document.getElementById(id);
}

//============= text ===============
////
//фокус на строке
function textGetFocus(num){
    //поменять рамку
    textGetObj(currentText).style.borderColor = unSelectColor;
    textGetObj(num).style.borderColor = isSelectColor;
    currentText = num;  //сменить значение выбрвнного элемента
    panelTextUpdate();  //обновить панель настроек

    tempTextSlide = slidingText[num];   //запомнить состояние строки
    slidingText[num] = false;           //остановить анимацию
    textSetValue(num, textValue[num]);  //вурнуть введенный текст
}
////
//строка потеряла фокус
function textLostFocus(){
    slidingText[currentText] = tempTextSlide;//вернуть состояние строки
    var str = textValue[currentText] = textGetValue(currentText);//запомнить введенный текст
    //дополнить строки пробелами во всю строку, если она короче
    var size = 20;
    str += ' ';
    if(str.length < size){
        for(var i=str.length; i<size; i++)
            str += ' ';
    }
    textSetValue(currentText, str); //записать полученную строку
    textSlideShow(currentText);     //запустить анимацию
}
// click on START/STOP button
function btnTextClicked(){
    //поменять состояние анимации
    slidingText[currentText] = !slidingText[currentText];
    btnTextUpdate();            //обновить кнопку
    textSlideShow(currentText); //запуск анимации
    document.onmouseout(document.getElementsByName('bTextStart')[0]);//убрать старую подсказку
}
// click on ROUTE button
function btnTextRoute() {
    stepText[currentText] *= -1;//поменять направление
    btnTextRouteUpdate();       //обновить кнопку
    document.onmouseout(document.getElementsByName('bTextRoute')[0]);//убрать старую подсказку
}
// обновить панель управления
function panelTextUpdate(){
    btnTextUpdate();
    btnTextRouteUpdate();
    textSetSpeed(speedText[currentText]);
    document.getElementById('currText').innerHTML="Бегущая строка ("+(currentText+1)+")";
}
// обноить кнопку запуска
function btnTextUpdate(){
    if(slidingText[currentText] == true){
        btnTextSetValue(btnValue[1], btnTooltipValue[1]);
    } else {
        btnTextSetValue(btnValue[0], btnTooltipValue[0]);
     }
}
// обновить кнопку направления
function btnTextRouteUpdate(){
    if(stepText[currentText] == 1){
        btnTextRouteSetValue(btnRoute[1], btnTooltipRoute[1]);
    } else {
        btnTextRouteSetValue(btnRoute[0], btnTooltipRoute[0]);
    }
}
// движение текста
function textSlideShow(num){
    if(slidingText[num] == true){
        if(timerIdText[num] != -1) //если сдвиг строки уже запланирован -
            clearTimeout(timerIdText[num]); // отменить
        timerIdText[num] = setTimeout(textSlideShow, 1000/speedText[num], num);
        var str = textGetValue(num);
        if(stepText[num] == 1)  //сдвигать строку в заданном направлении (1: прямо, -1: обратно)
            str = str.slice(1, str.length) + str[0];
        else
            str = str[str.length-1] + str.slice(0, str.length-1);
        textSetValue(num, str); //записать значение
    } else {
        timerIdText[num] = -1;  //пометить, что сдвиг не запланирован
    }
}

// changed speed of sliding text
function textInputSpeed(){
    var sp = textGetSpeed();    //получили введенную скорость

    var maxsp = 10, minsp = 1;
    if ((sp <= maxsp)&&(sp >= minsp)){
        setSpeed(sp);//устанавливаем, если это число
    } else if (sp > maxsp){//если больше - ставить максимум
        setSpeed(maxsp);
    } else if (sp < minsp){
        setSpeed(minsp);//если меньше - ставить минимум
    } else {//если не число - вписать старую скорость
        textSetSpeed(speedText[currentText]);
    }
    function setSpeed(speed){
        textSetSpeed(speed);
        speedText[currentText] = speed;
        textSlideShow(currentText);
    }
}

// geters & seters
function textSetSpeed(col){
    cpAnimate.tSpeed.value = col;
}
function textGetSpeed(){
    return Number(cpAnimate.tSpeed.value);
}
function textGetValue(num){
    return textGetObj(num).value;
}
function textSetValue(num, str){
    textGetObj(num).value = str;
}
function textGetObj(num){
    return document.getElementsByName('text'+num)[0];
}
function btnTextSetValue(str, tip){
    cpAnimate.bTextStart.value = str;
    cpAnimate.bTextStart.dataset.tooltip = tip;
}
function btnTextRouteSetValue(str, tip){
    cpAnimate.bTextRoute.value = str;
    cpAnimate.bTextRoute.dataset.tooltip = tip;
}
//------------- text ---------------

//============== img ===============
// click on START/STOP button
function btnImgClicked(){
    slidingImg[currentImg] = !slidingImg[currentImg];
    btnImgUpdate();
    imgSlideShow(currentImg);
    document.onmouseout(document.getElementsByName('bImgStart')[0]);
}
// click on ROUTE button
function btnImgRoute() {
    stepImg[currentImg] *= -1;  //сменить направление
    btnImgRouteUpdate();        //обновить кнопку
    document.onmouseout(document.getElementsByName('bImgRoute')[0]);
}
// click on Slide-show panel (picture)
function imgClicked(num){
    //обновить рамку
    imgGetBrdObj(currentImg).style.borderColor = unSelectColor;
    imgGetBrdObj(num).style.borderColor = isSelectColor;
    currentImg = num;   //заменить номер выбранного
    panelImgUpdate();   //обновить панель управления
}
// обновить панель управления
function panelImgUpdate(){
    btnImgUpdate();
    btnImgRouteUpdate();
    imgSetSpeed(speedImg[currentImg]);
    document.getElementById('currImg').innerHTML="Слайд-шоу ("+(currentImg+1)+")";
}
// обновить кнопку запуска
function btnImgUpdate(){
    if(slidingImg[currentImg] == true){
        btnImgSetValue(btnValue[1], btnTooltipValue[1]);
    } else {
        btnImgSetValue(btnValue[0], btnTooltipValue[0]);
    }
}
// обновить кнопку направления
function btnImgRouteUpdate(){
    if(stepImg[currentImg] == 1){
        btnImgRouteSetValue(btnRoute[0], btnTooltipRoute[0]);
    } else {
        btnImgRouteSetValue(btnRoute[1], btnTooltipRoute[1]);
    }
}
//button for add & delete file of current list
function addFile(){
    var url = cpAnimate.fileUrl.value; 
    if(url != '')
        imgValue[currentImg].push(url);
}
function delFile(){
    if(imgValue[currentImg].length > 0)
        if(getIndex(currentImg) == (imgValue[currentImg].length-1))
            nextImg(currentImg);
        imgValue[currentImg].pop();
}

// run slide-show
function imgSlideShow(num){
    if(slidingImg[num] == true){
        if(timerIdImg[num] != -1) //если смена кадра уже запланирована -
            clearTimeout(timerIdImg[num]); // отменить
        timerIdImg[num] = setTimeout(imgSlideShow, 5000/speedImg[num], num);
        nextImg(num);   //перелистнусть картинку
    } else {
        timerIdImg[num] = -1;//пометить, что не запланирована смена кадра
    }
}
//следующая картинка
function nextImg(num){
    if(imgValue[num].length != 0)
        imgSetValue(num, imgValue[num][(getIndex(num)+stepImg[num] + imgValue[num].length) % imgValue[num].length]);
    else {//если была удалена последняя, то нечего удалять
        imgSetValue(num, '');
        imgSlideShow(num);
        slidingImg[num] = false;
    }
}
//возвращает номер текущего слайда
function getIndex(num){
    url = imgGetValue(num);
    for(var p of imgValue[num]){
        if(url.indexOf(p) != -1){
            return Number(imgValue[num].indexOf(p));
        }
    }
    console.log("==!!!ERRROR of url index!!!==");
    return -1;
}
// changed speed of slide-show
function imgInputSpeed(){
    var sp = imgGetSpeed();    //получили введенную скорость
    
    var maxsp = 5, minsp = 1;
    if ((sp <= maxsp)&&(sp >= minsp)){
        setSpeed(sp);//устанавливаем, если это число
    } else if (sp > maxsp){//если больше - ставить максимум
        setSpeed(maxsp);
    } else if (sp < minsp){
        setSpeed(minsp);//если меньше - ставить минимум
    } else {//если не число - вписать старую скорость
        imgSetSpeed(speedImg[currentImg]);
    }
    function setSpeed(speed){
        imgSetSpeed(speed);
        speedImg[currentImg] = speed;
        imgSlideShow(currentImg);
    }
}
//geters & seters
function imgGetValue(num){
    return imgGetObj(num).src;
}//при изменении SRC картинки, меняем и подсказку к ней
function imgSetValue(num, url){
    imgGetObj(num).dataset.tooltip = imgGetObj(num).src = url;
}
function imgGetObj(num){
    return document.getElementsByName('img'+num)[0];
}
function imgGetBrdObj(num){
    return document.getElementById('img'+num);
}
function imgSetSpeed(col){
    cpAnimate.iSpeed.value = col;
}
function imgGetSpeed(){
    return Number(cpAnimate.iSpeed.value);
}
function btnImgSetValue(str, tip){
    cpAnimate.bImgStart.value = str;
    cpAnimate.bImgStart.dataset.tooltip = tip;
}
function btnImgRouteSetValue(str, tip){
    cpAnimate.bImgRoute.value = str;
    cpAnimate.bImgRoute.dataset.tooltip = tip;
}
//-------------- img ---------------


//============= time ===============
function startTime(){
    setTimeout(startTime, 1000);
    var tm=new Date();//создать объект текущего времени
    var h=tm.getHours();    //получить часы
    var m=checkTime(tm.getMinutes());//минуты
    var s=checkTime(tm.getSeconds());//секунды
    //обновить значение таймера
    document.getElementById('clocks').innerHTML=h+":"+m+":"+s;

    //приписывать нуль к однозначному числу
    function checkTime(i){
        if (i<10)
        i="0" + i;
        return i;
    }
}
//----------------------------------

// Скрипт для всплывающей подсказки
var tooltipElem;
// Наведение курсора на элемент
document.onmouseover = function(event) {
    var target = event.target;

    // если у нас есть подсказка...
    var tooltipHtml = target.dataset.tooltip;
    if (!tooltipHtml) 
        return;

    // ...создадим элемент для подсказки
    tooltipElem = document.createElement('div');
    tooltipElem.className = 'tooltip';
    tooltipElem.innerHTML = tooltipHtml;
    document.body.append(tooltipElem);

    // спозиционируем его сверху от аннотируемого элемента (top-center)
    var coords = target.getBoundingClientRect();

    var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
    if (left < 0) 
        left = 0; // не заезжать за левый край окна

    var top = coords.top - tooltipElem.offsetHeight - 5;
    if (top < 0) { // если подсказка не помещается сверху, то отображать её снизу
        top = coords.top + target.offsetHeight + 5;
    }

    tooltipElem.style.left = left + 'px';
    tooltipElem.style.top = top + 'px';
};
// Удаление подсказки при отведении курсора
document.onmouseout = function(e) {
    if (tooltipElem) {
        tooltipElem.remove();
        tooltipElem = null;
    }
};

// запуск тестового слайдшоу
function testSS(){
    for(var i=0; i<3; i++){
        slidingImg[i] = true;
        stepImg[i] = Math.pow(-1, i);
        imgSlideShow(i);
        // console.log("ss: "+slidingImg[i]+" st: "+stepImg[i])
    }
    panelImgUpdate();
}

//----------------- THE END ----------------------
function test(){
    // nextImg(0);
    // console.log(imgGetValue(0));
    // alert(imgGetValue(0));
    // console.log(document.getElementsByClassName('imClass'));
    // console.log(document.getElementById('imgBox'));
    // console.log(getSubmite());
    // console.log(getAddition());
    // console.log(getAddition().files[0]);
    // console.log(document.getElementsByName('img0')[0].src);
    // console.log(imgg);
    // console.log(slidingText);
    // cpAnimate.cbStart.checked = false;
    
}
