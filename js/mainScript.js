// классы??

//TODO: грамотное движение текста
//TODO: обновление фокуса при наведении
//      а пауза старт при клике

//запуск при открытии страницы
function bodyStart(){
    // console.log('start body');
    startTime();//запуск таймера
    
    //setup TEXT props
    var myTextList = [
        "При наведении на кнопки и слайды вылезают подсказки!", 
        "123456789 10 11", 
        "abcd"
    ];
    console.log(getByName('bTextStart'));
    textPanel = new ControlPanel(
        getByName('bTextStart'), 
        getByName('bTextRoute'), 
        getById('currText'), 
        getByName('tSpeed'), 10, 1
    );
    for(var i=0; i<3; i++){
        textPanel.addItem(
            new TextSlideShow(getByName("text"+i)), 
            10-i*4, 
            true, 
            Math.pow(-1, i), 
            myTextList[i]
        );
        textGetFocus(i);    //установить фокус (чтобы заполнить строку и обновить панель)
        textLostFocus();    //убрать фокус (чтобы запустить анимацию)
    }
    console.log('-=-=-=-=-=-=-=-=-=-=-');

    //setup IMG props
    var imgValue = [
        ["img/01/image1.jpg", "img/01/image2.jpg", "img/01/image3.jpg", "img/01/image10.jpg"],
        ["img/02/image7.jpg", "img/02/image9.jpg"],
        ["img/03/image5.jpg", "img/03/image4.jpg", "img/03/image6.jpg", "img/03/image8.jpg"]
    ];
    imgPanel = new ImgControlPanel(
        getByName('bImgStart'),     //
        getByName('bImgRoute'),     //
        getById('currImg'),         //
        getByName('iSpeed'), 5, 1,  //
        getByName('fileUrl'),       //
        getByName('bAddFile'),      //
        getByName('bDelFile')       //
    );
    for(var i=0; i<3; i++){
        imgPanel.addItem(
            new ImgSlideShow(getById('img'+i)), 
            4-i*1.5, 
            false, 
            Math.pow(-1, i), 
            imgValue[i]
        );
        // imgPanel.tryAnimation();
    }
    // imgClicked(0);                      //переместить фокус на первый слайд
    
    // item = new SlideShowItem("hello");
    // item.setSpeed(999);
    // item2 = new SlideShowItem("second");
    // img.test();
    // console.log(text);
    console.log('== body start ==');
}

function getByName(name){
    return document.getElementsByName(name)[0];
}
function getById(id){
    return document.getElementById(id);
}

//============= text ===============
//фокус на строке
function textGetFocus(num){
    // console.log('get focus');
    //поменять фокус, рамку и панель
    textPanel.select(num);
    //запомнить состояние строки
    tempSlidingText = textPanel.getCurrentSliding();
    textPanel.setCurrentSliding(false);//остановить анимацию
    textPanel.updateItemValue();       //вeрнуть введенный текст в исходное положение
}
//строка потеряла фокус
function textLostFocus(){
    // console.log('lost focus');
    textPanel.setCurrentSliding(tempSlidingText);//вернуть состояние строки
    textPanel.saveCurrentValue();
    var str = textPanel.getCurrentValue();//запомнить введенный текст
    //дополнить строки пробелами во всю строку, если она короче
    var size = 20;
    str += ' ';
    if(str.length < size){
        for(var i=str.length; i<size; i++)
            str += ' ';
    }
    textPanel.setCurrentValue(str); //записать полученную строку
    textPanel.tryAnimation();     //запустить анимацию
}
// click on START/STOP button
function btnTextClicked(){
    //поменять состояние анимации
    textPanel.eventBtnStartClicked();
    // slidingText[currentText] = !slidingText[currentText];
    // btnTextUpdate();            //обновить кнопку
    // textSlideShow(currentText); //запуск анимации
    // document.onmouseout(document.getElementsByName('bTextStart')[0]);//убрать старую подсказку
}
// click on ROUTE button
function btnTextRoute() {
    textPanel.eventBtnRouteClicked();
    // stepText[currentText] *= -1;//поменять направление
    // btnTextRouteUpdate();       //обновить кнопку
    // document.onmouseout(document.getElementsByName('bTextRoute')[0]);//убрать старую подсказку
}
// обновить панель управления
// function panelTextUpdate(){
    //     btnTextUpdate();
    //     btnTextRouteUpdate();
    //     textSetSpeed(speedText[currentText]);
    //     document.getElementById('currText').innerHTML="Бегущая строка ("+(currentText+1)+")";
    // }
    // обноить кнопку запуска
// function btnTextUpdate(){
//     if(slidingText[currentText] == true){
//         btnTextSetValue(btnValue[1], btnTooltipValue[1]);
//     } else {
//         btnTextSetValue(btnValue[0], btnTooltipValue[0]);
//      }
// }
// обновить кнопку направления
// function btnTextRouteUpdate(){
//     if(stepText[currentText] == 1){
//         btnTextRouteSetValue(btnRoute[1], btnTooltipRoute[1]);
//     } else {
//         btnTextRouteSetValue(btnRoute[0], btnTooltipRoute[0]);
//     }
// }
// движение текста
// function textSlideShow(num){
//     if(slidingText[num] == true){
//         if(timerIdText[num] != -1) //если сдвиг строки уже запланирован -
//             clearTimeout(timerIdText[num]); // отменить
//         timerIdText[num] = setTimeout(textSlideShow, 1000/speedText[num], num);
//         var str = textGetValue(num);
//         if(stepText[num] == 1)  //сдвигать строку в заданном направлении (1: прямо, -1: обратно)
//             str = str.slice(1, str.length) + str[0];
//         else
//             str = str[str.length-1] + str.slice(0, str.length-1);
//         textSetValue(num, str); //записать значение
//     } else {
//         timerIdText[num] = -1;  //пометить, что сдвиг не запланирован
//     }
// }

// changed speed of sliding text
function textInputSpeed(){
    textPanel.eventInputSpeed();
    // var sp = textGetSpeed();    //получили введенную скорость

    // var maxsp = 10, minsp = 1;
    // if ((sp <= maxsp)&&(sp >= minsp)){
    //     setSpeed(sp);//устанавливаем, если это число
    // } else if (sp > maxsp){//если больше - ставить максимум
    //     setSpeed(maxsp);
    // } else if (sp < minsp){
    //     setSpeed(minsp);//если меньше - ставить минимум
    // } else {//если не число - вписать старую скорость
    //     textSetSpeed(speedText[currentText]);
    // }
    // function setSpeed(speed){
    //     textSetSpeed(speed);
    //     speedText[currentText] = speed;
    //     textSlideShow(currentText);
    // }
}

// geters & seters
// function textSetSpeed(col){
//     cpAnimate.tSpeed.value = col;
// }
// function textGetSpeed(){
//     return Number(cpAnimate.tSpeed.value);
// }
// function textGetValue(num){
//     return textGetObj(num).value;
// }
// function textSetValue(num, str){
//     textGetObj(num).value = str;
// }
// function textGetObj(num){
//     return document.getElementsByName('text'+num)[0];
// }
// function btnTextSetValue(str, tip){
//     cpAnimate.bTextStart.value = str;
//     cpAnimate.bTextStart.dataset.tooltip = tip;
// }
// function btnTextRouteSetValue(str, tip){
//     cpAnimate.bTextRoute.value = str;
//     cpAnimate.bTextRoute.dataset.tooltip = tip;
// }
//------------- text ---------------

//============== img ===============
// click on START/STOP button
function btnImgClicked(){
    imgPanel.eventBtnStartClicked();
    // slidingImg[currentImg] = !slidingImg[currentImg];
    // btnImgUpdate();
    // imgSlideShow(currentImg);
    // document.onmouseout(document.getElementsByName('bImgStart')[0]);
}
// click on ROUTE button
function btnImgRoute() {
    imgPanel.eventBtnRouteClicked();
    // stepImg[currentImg] *= -1;  //сменить направление
    // btnImgRouteUpdate();        //обновить кнопку
    // document.onmouseout(document.getElementsByName('bImgRoute')[0]);
}
// click on Slide-show panel (picture)
function imgClicked(num){
    //обновить рамку
    imgPanel.select(num);
    // imgGetBrdObj(currentImg).style.borderColor = unSelectColor;
    // imgGetBrdObj(num).style.borderColor = isSelectColor;
    // currentImg = num;   //заменить номер выбранного
    // panelImgUpdate();   //обновить панель управления
}
////
// обновить панель управления
// function panelImgUpdate(){
//     btnImgUpdate();
//     btnImgRouteUpdate();
//     imgSetSpeed(speedImg[currentImg]);
//     document.getElementById('currImg').innerHTML="Слайд-шоу ("+(currentImg+1)+")";
// }
// обновить кнопку запуска
// function btnImgUpdate(){
//     if(slidingImg[currentImg] == true){
//         btnImgSetValue(btnValue[1], btnTooltipValue[1]);
//     } else {
//         btnImgSetValue(btnValue[0], btnTooltipValue[0]);
//     }
// }
// обновить кнопку направления
// function btnImgRouteUpdate(){
//     if(stepImg[currentImg] == 1){
//         btnImgRouteSetValue(btnRoute[0], btnTooltipRoute[0]);
//     } else {
//         btnImgRouteSetValue(btnRoute[1], btnTooltipRoute[1]);
//     }
// }
//button for add & delete file of current list
function addFile(){
    imgPanel.eventAddFile();
    // var url = cpAnimate.fileUrl.value; 
    // if(url != '')
    //     imgValue[currentImg].push(url);
}
function delFile(){
    imgPanel.eventDelFile();
    // if(imgValue[currentImg].length > 0)
    //     if(getIndex(currentImg) == (imgValue[currentImg].length-1))
    //         nextImg(currentImg);
    // imgValue[currentImg].pop();
}

// run slide-show
// function imgSlideShow(num){
//     if(slidingImg[num] == true){
//         if(timerIdImg[num] != -1) //если смена кадра уже запланирована -
//             clearTimeout(timerIdImg[num]); // отменить
//         timerIdImg[num] = setTimeout(imgSlideShow, 5000/speedImg[num], num);
//         nextImg(num);   //перелистнусть картинку
//     } else {
//         timerIdImg[num] = -1;//пометить, что не запланирована смена кадра
//     }
// }
//следующая картинка
// function nextImg(num){
//     if(imgValue[num].length != 0)
//         imgSetValue(num, imgValue[num][(getIndex(num)+stepImg[num] + imgValue[num].length) % imgValue[num].length]);
//     else {//если была удалена последняя, то нечего удалять
//         imgSetValue(num, '');
//         imgSlideShow(num);
//         slidingImg[num] = false;
//     }
// }
//возвращает номер текущего слайда
// function getIndex(num){
//     url = imgGetValue(num);
//     for(var p of imgValue[num]){
//         if(url.indexOf(p) != -1){
//             return Number(imgValue[num].indexOf(p));
//         }
//     }
//     console.log("==!!!ERRROR of url index!!!==");
//     return -1;
// }
// changed speed of slide-show
function imgInputSpeed(){
    console.log('inoputtttt');
    imgPanel.eventInputSpeed();
    // var sp = imgGetSpeed();    //получили введенную скорость
    
    // var maxsp = 5, minsp = 1;
    // if ((sp <= maxsp)&&(sp >= minsp)){
    //     setSpeed(sp);//устанавливаем, если это число
    // } else if (sp > maxsp){//если больше - ставить максимум
    //     setSpeed(maxsp);
    // } else if (sp < minsp){
    //     setSpeed(minsp);//если меньше - ставить минимум
    // } else {//если не число - вписать старую скорость
    //     imgSetSpeed(speedImg[currentImg]);
    // }
    // function setSpeed(speed){
    //     imgSetSpeed(speed);
    //     speedImg[currentImg] = speed;
    //     imgSlideShow(currentImg);
    // }
}
//geters & seters
// function imgGetValue(num){
//     return imgGetObj(num).src;
// }//при изменении SRC картинки, меняем и подсказку к ней
// function imgSetValue(num, url){
//     imgGetObj(num).dataset.tooltip = imgGetObj(num).src = url;
// }
// function imgGetObj(num){
//     return document.getElementsByName('img'+num)[0];
// }
// function imgGetBrdObj(num){
//     return document.getElementById('img'+num);
// }
// function imgSetSpeed(col){
//     cpAnimate.iSpeed.value = col;
// }
// function imgGetSpeed(){
//     return Number(cpAnimate.iSpeed.value);
// }
// function btnImgSetValue(str, tip){
//     cpAnimate.bImgStart.value = str;
//     cpAnimate.bImgStart.dataset.tooltip = tip;
// }
// function btnImgRouteSetValue(str, tip){
//     cpAnimate.bImgRoute.value = str;
//     cpAnimate.bImgRoute.dataset.tooltip = tip;
// }
//-------------- img ---------------


//============= time ===============
function startTime(){
    setTimeout(startTime, 1000);
    var tm=new Date();//создать объект текущего времени
    var h=tm.getHours();    //получить часы
    var m=checkTime(tm.getMinutes());//минуты
    var s=checkTime(tm.getSeconds());//секунды
    //обновить значение таймера
    document.getElementById('clocks').innerHTML = h+":"+m+":"+s;

    //приписывать нуль к однозначному числу
    function checkTime(i){
        if (i<10)
        i="0" + i;
        return i;
    }
}
//----------------------------------

// Скрипт для всплывающей подсказки
var tooltipElem;
// Наведение курсора на элемент
document.onmouseover = function(event) {
    var target = event.target;

    // если у нас есть подсказка...
    var tooltipHtml = target.dataset.tooltip;
    if (!tooltipHtml) 
        return;

    // ...создадим элемент для подсказки
    tooltipElem = document.createElement('div');
    tooltipElem.className = 'tooltip';
    tooltipElem.innerHTML = tooltipHtml;
    document.body.append(tooltipElem);

    // спозиционируем его сверху от аннотируемого элемента (top-center)
    var coords = target.getBoundingClientRect();

    var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
    if (left < 0) 
        left = 0; // не заезжать за левый край окна

    var top = coords.top - tooltipElem.offsetHeight - 5;
    if (top < 0) { // если подсказка не помещается сверху, то отображать её снизу
        top = coords.top + target.offsetHeight + 5;
    }

    tooltipElem.style.left = left + 'px';
    tooltipElem.style.top = top + 'px';
};
// Удаление подсказки при отведении курсора
document.onmouseout = function(e) {
    if (tooltipElem) {
        tooltipElem.remove();
        tooltipElem = null;
    }
};

// запуск тестового слайдшоу
function testSS(){
    for(var i=0; i<3; i++){
        imgPanel.select(i);
        // imgPanel.setCurrentSliding(true);
        imgPanel.eventBtnStartClicked();
        // slidingImg[i] = true;
        // stepImg[i] = Math.pow(-1, i);
        imgPanel.tryAnimation();
        // imgSlideShow(i);
        // console.log("ss: "+slidingImg[i]+" st: "+stepImg[i])
    }
}

//----------------- THE END ----------------------
function test(){
    // nextImg(0);
    // console.log(imgGetValue(0));
    // alert(imgGetValue(0));
    // console.log(document.getElementsByClassName('imClass'));
    // console.log(document.getElementById('imgBox'));
    // console.log(getSubmite());
    // console.log(getAddition());
    // console.log(getAddition().files[0]);
    // console.log(document.getElementsByName('img0')[0].src);
    // console.log(imgg);
    // console.log(slidingText);
    // cpAnimate.cbStart.checked = false;
    
}
